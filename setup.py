from setuptools import setup

setup(
    name='askap_tools',
    version='0.1',
    py_modules=['cut_mosaic'],
    install_requires=[
        'Click',
    ],
    entry_points={
        'console_scripts': [
            'cut_mosaic=cut_mosaic:cli'
        ]},
)
