import click
import os
from at_fncts import make_png, select_input_objects, make_cutout
import ibcommon.parse as ibparse


@click.command()
@click.argument('input_image', nargs=1)
@click.argument('results_path', nargs=1)
@click.option('--obj_id', '-i', default='all')
@click.option('--img_name_suffix', '-s', default='cut')
@click.option('--image_size', '-z', default=120)
@click.option('--make_pngs', '-m', is_flag=True, default=False)
def cli(input_image, results_path, obj_id, make_pngs, img_name_suffix, image_size):
    # convert types
    image_size = float(image_size)

    input_objects = select_input_objects(obj_id)

    for curr_object in input_objects:

        # define and create folder for cutouts and pngs
        # in the form result_path/idpnmain/
        cutouts_path = "{}{}/".format(ibparse.corrpath(results_path), curr_object.idpnmain)
        if not os.path.exists(cutouts_path):
            os.makedirs(cutouts_path)

        out_image = "{}{}_{}.fits".format(ibparse.corrpath(cutouts_path), curr_object.idpnmain, img_name_suffix)

        # TODO check if image exists

        make_cutout(input_image, out_image, curr_object, image_size)

        if make_pngs and os.path.exists(out_image):
            make_png(out_image, curr_object, cutouts_path, img_name_suffix)


