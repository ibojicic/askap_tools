# import datetime
import sys
import click
# import ibcommon.parse as ib_parse
from ibcommon import dicts as ibdicts
import ibcommon.parse as ib_parse
import ibplotting.plotlibs as ib_plot
from astropy import units as u
from astropy.coordinates import SkyCoord

from CutoutsClass import Cutouts

from pprint import pprint
import os
from dbModels_MainGPN import MainGPN
from dbModels_MCs import LmcAegeanCatalogue
import subprocess
import csv
import numpy as np
# import pandas as pd
# import ibastro.montages as ib_montages
from ibcommon.parse import corrpath

# import ibplotting.plotlibs as ibplot

fits_files = {
    "lmc_askap": "/Users/ibojicic/data/fitsImages/ASKAP/LMC_observations-8532-image_cubes-1373.fits"
}


detected = [7162, 7194, 7196, 7216, 7217, 7244, 7245, 7262, 7265, 7267, 7268, 7270, 7271, 7323, 7327, 7328, 7330,
            7332, 7395, 7396, 7397, 7398, 7399, 7426, 7427, 7428, 7450, 7451, 7452, 7453, 7462, 7471, 7472, 7473,
            7474, 7537, 7539, 7541, 7542, 7543, 7544, 7545, 7546, 7547, 7553, 7571, 7584, 7585, 7586, 7587, 7597,
            7598, 7599, 7600, 7616, 7619, 7620, 7622, 7624, 7659, 7660, 7673, 7703, 7713, 7714, 7715, 7717, 7718,
            7720, 7721, 7722, 7723, 7724, 7725, 7726, 7728, 7729, 7731, 7732, 7733, 7734, 7735, 7736, 7737, 7738,
            7739, 7770, 7803, 7808, 7809, 5095, 5099, 5101, 5104, 5107, 5109, 5112, 5113, 5114, 5116, 5120, 5121,
            5122, 5123, 5125, 5127, 5128, 5131, 5132, 5135, 5137, 5139, 5148, 5149, 5150, 5152, 5153, 5154, 5156,
            5159, 5161, 5163, 5166, 5167, 5169, 7220, 7238, 7313, 7389, 7411, 7413, 7509, 7581, 7702]


@click.command()
@click.argument('results_path', nargs=1)
@click.argument('prefix', nargs=1)
@click.option('--obj_id', '-i', default='all')
@click.option('--rewrite', '-r', is_flag=True, default=False)
@click.option('--record_to_db', '-d', is_flag=True, default=False)
@click.option('--seedclip', '-s', default=4)
@click.option('--floodclip', '-f', default=3)
def cli(obj_id, results_path, prefix, rewrite, record_to_db, seedclip, floodclip):
    intable = MainGPN

    outtable = LmcAegeanCatalogue

    input_objects = intable.select(intable.idpnmain,
                                   intable.draj2000,
                                   intable.ddecj2000,
                                   intable.pnstat).where(intable.idpnmain << detected)

    if obj_id != 'all':
        input_objects = input_objects.where(intable.idpnmain == obj_id)

    for curr_object in input_objects:
        print(curr_object.idpnmain)
        object_path = "{}{}/".format(ib_parse.corrpath(results_path),
                                     curr_object.idpnmain)

        exit_flag = True
        n = 0

        curr_seedclip = seedclip
        curr_floodclip = floodclip
        inimage, out_table, comp_table, mimas_region, png_image, zoom_fits, zoom_png = \
            file_names(object_path, curr_object.idpnmain, prefix)

        while exit_flag and curr_floodclip > 1:
            n = n + 1

            photres = aegean_phot(curr_object, curr_seedclip, curr_floodclip, mimas_region, inimage, comp_table,
                                  out_table)

            if not photres:
                photres = [
                    {'name': curr_object.idpnmain}
                ]
                exit_flag = False

            if len(photres) > 1:
                photres = find_closest(photres)

            print(curr_seedclip)
            print(curr_floodclip)
            print(photres[0]['offset_fit'])

            curr_seedclip = curr_seedclip - 0.5
            curr_floodclip = curr_floodclip - 0.5

            if float(photres[0]['offset_fit']) < float(photres[0]['psf_a']) / 2.:
                exit_flag = False

        if not exit_flag:
            outtable.create(**photres[0])
            make_png(inimage, curr_object, png_image, photres[0],
                     add_comments="{}; {}".format(curr_object.idpnmain, curr_object.pnstat))
            make_zoom(inimage,results_path,curr_object)
            # make_png(zoom_fits, curr_object, zoom_png, photres[0])
